### GeoLocation - React project

## Technologies used:

- React
- JavaScript
- HTML
- BootStrap
- CSS
- match-sorter

## The brief was to:

- The objective is to provide a user interface to search for names.
- The interface should comprise a search box and list of results.
- When the user starts typing the results should be displayed in the list below.

The site can be run locally by cloning the repository and installing the dependencies by typing yarn. To start the server, type yarn start:server in the terminal and to render the front-end, type yarn start.

## App overview

The application allows a user to search for a location by name, returning the results (name and coordinates) under the search input. The app uses a third-party package called match-sorter to sort the locations by closest match.

# Desktop view:
<div align="center">
<img src="https://res.cloudinary.com/dhrxw6zhp/image/upload/v1637932100/Screenshot_2021-11-25_at_14.56.00_cix408.png" width="500px" height="500px"/>

<img src="https://res.cloudinary.com/dhrxw6zhp/image/upload/v1637932094/Screenshot_2021-11-25_at_14.58.25_v1ovrp.png" width="500px" height="500px"/>
</div>

# Mobile view: 
<div align="center">
<img src="https://res.cloudinary.com/dhrxw6zhp/image/upload/v1637932086/Screenshot_2021-11-25_at_18.19.43_p0yefh.png" width="500px" height="650px"/>
</div>
