# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```
The output of the following code is 2 followed by 1. This is because we have used the ‘setTimeout’ function which allows you to set a timer for when you want the function to execute. In this case console.log(2) will be executed instantly, and console.log(1) after 100 milliseconds. 




Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```
The output of the following code is 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10. This is because the (recursive) function consists of a if statement that says if the argument (d) being passed is less than 10 then call foo(d+1). This means 0+1, 1+1, 2+1, and so on up until 9+1 are passed as arguments to the foo function. But outside of the if statement there is a console.log(d) which is called after foo(d+1) however, is quicker.




Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```
Instead, you could define foo as foo(d=5) { console.log(d) }. However im not sure what is wrong with this solution as it would provide d =5 if nothing was passed through




Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```
The output of the following code is 3. 
This is because bar is the following `const bar = (b) => return 1 + b`.
So doing `bar(2) = 1 + 2` which is `3`. 




Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```
The function expects two arguments. 
The first a, is a number otherwise multiplying it by two in `done(a * 2)` would throw an error.
The second argument `done` is a function, which will be called with argument `a * 2` after 100 milliseconds. 



