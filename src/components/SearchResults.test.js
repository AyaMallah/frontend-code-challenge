import React from 'react'
import { render, screen } from '@testing-library/react'
import SearchResult from './SearchResults';


describe('SearchResults Component', () => {

  test('Component render without crashing when locations.length = 0', () => {
    const mockLocation = []
    render(<SearchResult locations={mockLocation} />)
    expect(screen.getByText('No Locations')).toBeInTheDocument()
  })

  test('Component render without crashing when locations.length > 0', () => {
    const mockLocation = [{
      geonameid: 200,
      name: 'London',
      latitude: 2,
      longitude: 3
    },
    {
      geonameid: 250,
      name: 'Hackney',
      latitude: 5,
      longitude: 2
    },
    {
      geonameid: 300,
      name: 'Acton',
      latitude: 4,
      longitude: 7
    }]

    render(<SearchResult locations={mockLocation} />)
    expect(screen.queryByText('No Locations')).not.toBeInTheDocument()
  })

})