import React from 'react'
import { render, screen } from '@testing-library/react'
import Card from './Card';


describe('Card Component', () => {

  const mockLocation = {
    geonameid: 200,
    name: 'London',
    latitude: 2,
    longitude: 3
  }

  test('renders App without crashing', () => {
    render(<Card location={mockLocation} />)
    screen.debug()
  })

  test('renders the location name', () => {
    render(<Card location={mockLocation} />)
    expect(screen.getByText(mockLocation.name)).toBeInTheDocument()
  })

  test('renders the location coordinates', () => {
    render(<Card location={mockLocation} />)
    expect(screen.getByText(mockLocation.latitude)).toBeInTheDocument()
    expect(screen.getByText(mockLocation.longitude)).toBeInTheDocument()
  })

})