import React from 'react'
import { render, screen } from '@testing-library/react'
import App from './App';


describe('App Component', () => {

  test('renders App without crashing', () => {
    render(<App />)
  })

  test('renders the searchBar component', () => {
    render(<App />)
    expect(screen.getByTestId('searchBar')).toBeInTheDocument()
  })

  test('renders the search result component', () => {
    render(<App />)
    expect(screen.getByTestId('searchResults')).toBeInTheDocument()
  })

})