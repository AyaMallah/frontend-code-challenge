import React from 'react'
import '../styles/Card.css'

function Card({ location }) {
  return (
    <div className="card">
      <div>
        <p>{location.name}</p>
      </div>
      <div>
        <h6>Coords: </h6>
        <div className="coords">
          <p className="coord">{Math.round(location.latitude)}</p>
          <p className="coord">{Math.round(location.longitude)}</p>
        </div>
      </div>
    </div>
  )
}
export default Card
