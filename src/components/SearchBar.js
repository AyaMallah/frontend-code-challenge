import React from 'react'
import { Form } from 'react-bootstrap'
import '../styles/SearchBar.css'

const SearchBar = ({ setSearchTerm }) => {
  return (
    <Form data-testid="searchBar" className="searchBar">
      <Form.Control type="text" placeholder="Search Location..." onChange={(e) => setSearchTerm(e.target.value)} />
    </Form>
  )
}

export default SearchBar
