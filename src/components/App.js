import React, { useEffect, useState } from 'react';
import { matchSorter } from 'match-sorter';

import SearchBar from './SearchBar';
import SearchResults from './SearchResults';

import '../styles/App.css'

function App() {
  const [searchTerm, setSearchTerm] = useState('')
  const [locations, setLocations] = useState([])

  useEffect(() => {
    if (searchTerm.length > 1) {
      fetch(`http://localhost:3000/locations?q=${searchTerm}`)
        .then(response => response.json())
        .then(json => {
          const sortedJson = matchSorter(json, searchTerm, { keys: ['name'] })
          setLocations(sortedJson)
        })
    } else {
      setLocations([])
    }
  },
    [searchTerm]);

  return (
    <div className="main-container">
      <SearchBar setSearchTerm={setSearchTerm} />
      <SearchResults locations={locations} />
    </div>
  );
}

export default App;
