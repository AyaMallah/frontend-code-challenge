import React from 'react'
import Card from './Card'
import '../styles/SearchResults.css'

const SearchResult = ({ locations }) => {
  return (
    <div data-testid="searchResults" className="searchResults">
      {locations.length > 0 ?
        <div >
          {
            locations.map(location => (
              <div key={location.geonameid}>
                <Card location={location} />
              </div>
            ))
          }
        </div>
        :
        <div>No Locations</div>
      }
    </div>
  )
}

export default SearchResult
